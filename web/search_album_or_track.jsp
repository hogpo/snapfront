<%-- 
    Document   : index
    Created on : 29-Dec-2015, 12:46:45
    Author     : ACER-LAPTOP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search for album/track</title>
        <link rel="stylesheet" type="text/css" href="newcss.css">
    </head>
    <body>
        <div>Search for album/track</div>
        
      <form action="${pageContext.request.contextPath}/SearchAndDisplayAlbumOrTrackServlet">
               
        <p>

            <input type='text' name='search' maxlength="30" minlength='1' required />
            <input type="submit" name='track' value="Search track" />
            <input type="submit" name='album' value="Search album" />

        </p>
        
        </form>
    </body>
</html>
