<%-- 
    Document   : index
    Created on : 29-Dec-2015, 12:46:45
    Author     : ACER-LAPTOP
--%>

<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit track</title>
        <link rel="stylesheet" type="text/css" href="newcss.css">
    </head>
    <body>
        
        <h1>Edit track</h1>
        
        <form action="${pageContext.request.contextPath}/PostEditTrackServlet">
            
            <%
              
                    // get jsonObj & tracskArray
                    
                    JSONObject jsonObj = (JSONObject) request.getAttribute("jsonObj");
                    
                    String trackTitle = jsonObj.getString("title");
                    String length = jsonObj.getString("length");
                   
                   String trackIdString = jsonObj.getString("id");
                    String albumIdString = jsonObj.getString("album_id");
                                        
                    out.print("<input type=hidden name='album_id' value='" 
                            + albumIdString + "' />");  

                    out.print("<input type=hidden name='track_id' value='" 
                                                + trackIdString + "' />"); 

            %>
            
            <p>
                Current name: <% out.print(trackTitle); %>
                <input type="input" name="edit_name" max="50" />
                <input type="submit" name="editing_name" value="Edit name only" />
            </p>
            
            <p>
                Current length: <% out.print(length); %>
                <input type="number" min="0" max="999" name="edit_length" />
                <input type="submit" name="editing_length" value="Edit length only" />
            </p>
            
            <input type="submit" name="editing_both" value="Edit both name & length" />
            
            <p>
                <input type="submit" name="delete" value="Delete track" />
            </p>
            
            <!--
               (- could-do - delete album and not tracks)
            -->
            
       </form>
        
       </body>
</html>
