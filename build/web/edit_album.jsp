<%-- 
    Document   : index
    Created on : 29-Dec-2015, 12:46:45
    Author     : ACER-LAPTOP
--%>

<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit album</title>
        <link rel="stylesheet" type="text/css" href="newcss.css">
    </head>
    <body>
        
      
        <h1>Edit album</h1>
        
        <form action="${pageContext.request.contextPath}/PostEditAlbumServlet">
            
            <%
              
                    // get jsonObj & tracskArray
                    
                    JSONObject jsonObjBigger = (JSONObject) request.getAttribute("selectedAlbum");
                    
                    JSONObject jsonObj = (JSONObject) jsonObjBigger.get("album");
                    
                   // String albumTitle = obj.getString("title");
                   
                   String albumIdString = jsonObj.getString("id");
                    String albumName = jsonObj.getString("title");
                                        
                    out.print("<input type=hidden name='album_id' value='" 
                            + albumIdString + "' />");  

            %>
            
            <p>
                Current name: <% out.print(albumName); %>
                <input type="input" name="edit_name" max="50" />
                <input type="submit" name="editing_name" value="Edit name" />
            </p>
            
            <p>
                <input type="submit" name="delete" value="Delete album (& any tracks)" />
            </p>
            
            <!--
               (- could-do - delete album and not tracks)
            -->
            
        </form>
        
        <form action="${pageContext.request.contextPath}/PreEditTrackServlet">
            
            <p>
                
                <%
                    
                    JSONArray jsonTrackArray = jsonObj.getJSONArray("tracks");
                    
                    out.print("<input type=hidden name='jsonArrayLength' value='" 
                            + jsonTrackArray.length() + "' />");
                    
                    out.print("<input type=hidden name='album_id' value='" 
                            + albumIdString + "' />");  
                                        
                    // iterate over tracks and print out
                    
                    %>
                
                
                <table>
                    <tr>
                      <td><strong>Track #</strong></td>
                      <td><strong>Title</strong></td>		
                      <td><strong>Length</strong></td>
                      <td><strong>Edit?</strong></td>
                    </tr>
                
                
                <%

                    for (int i = 0; i < jsonTrackArray.length(); i++)
                    {

                        JSONObject jsonTrackObj = jsonTrackArray.getJSONObject(i);

                        String trackTitle = jsonTrackObj.getString("title");
                        String trackLength = jsonTrackObj.getString("length");

                        
                        int iPlusOne = i + 1;
                        
                        int trackId = jsonTrackObj.getInt("id");

                        out.print("<tr>");
                            out.print("<td>" + iPlusOne + "</td>");
                            out.print("<td>" + trackTitle + "</td>");
                            out.print("<td>" + trackLength + "</td>");
                            out.print("<td>" + "<input type=submit name='" + i 
                                    + "' value='Edit track' />" + "</td>");
                      
                                             
                        out.print("</tr>");
                        
                        //move if necessary
                        out.print("<input type=hidden name='track_" + i 
                                    + "' value='" + trackTitle + "' />");                
                            out.print("<input type=hidden name='id_" + i 
                                    + "' value='" + trackId + "' />"); 
                    }

                %>
                
                </table>
                
            </p>
        
        </form>
    </body>
</html>
