<%-- 
    Document   : index
    Created on : 29-Dec-2015, 12:46:45
    Author     : ACER-LAPTOP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add track</title>
        <link rel="stylesheet" type="text/css" href="newcss.css">
    </head>
    <body>
        <h1>Add track:</h1>
        
      <form action="${pageContext.request.contextPath}/PostAddTrackServlet">
               
          <p>
         Enter details for new track here: 
          
          </p>
          
          <p>
              <input type="text" name='track_name' maxlength="30" minlength="1" required/>
              Part of album: 
              <input type="text" name='album_name' maxlength="30" minlength="1" required/>
          </p>
          
          <!-- 
          
            COULDDO:
                
                - dynamic 'add another' track info                  
          
          -->
          
          
          <p>

            <input type="submit" />

        </p>
        
        </form>
    </body>
</html>