<%-- 
    Document   : index
    Created on : 29-Dec-2015, 12:46:45
    Author     : ACER-LAPTOP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Options page</title>
        <link rel="stylesheet" type="text/css" href="newcss.css">
    </head>
    <body>    
        
        <h1>
            Main options            
        </h1>
        
        <p>
                <a href="${pageContext.request.contextPath}/PreDisplayAlbumsServlet">
                    Display all albums/tracks (to edit)</a>
        </p>
        
        <p>
                <a href="${pageContext.request.contextPath}/add_album.jsp">Add an album</a>
        </p>
        
        <p>
                <a href="${pageContext.request.contextPath}/add_track.jsp">Add a track (to an album)</a>
        </p>
        
        <p>
                <a href="${pageContext.request.contextPath}/search_album_or_track.jsp">Search albums/tracks</a>
        </p>
        
    </body>
</html>
