<%-- 
    Document   : results
    Created on : 29-Dec-2015, 12:59:35
    Author     : ACER-LAPTOP
--%>

<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Display Albums</title>
        <link rel="stylesheet" type="text/css" href="newcss.css">
    </head>
    <body>
        
        <form action="${pageContext.request.contextPath}/PreEditAlbumServlet">
      
        
        <%

            JSONArray jsonArray = (JSONArray) request.getAttribute("jsonArray");
              
            out.print("<input type=hidden name='jsonArrayLength' "
                    + "value='" + jsonArray.length() + "' />");
            
            for (int i = 0; i < jsonArray.length(); i++)
            {
                // print out album name
                
                JSONObject jsonObj = jsonArray.getJSONObject(i);
                
                String albumTitle = jsonObj.getString("title");
                
                int albumId = jsonObj.getInt("id");
                
                int iPlusOne = i + 1;
                
                out.print("<h3>Album #" + iPlusOne + ": " + albumTitle + "</h3>");
                out.print("<input type=submit name='" + i 
                        + "' value='Edit album' />");
                out.print("<input type=hidden name='album_" + i 
                        + "' value='" + albumTitle + "' />");                
                out.print("<input type=hidden name='id_" + i 
                        + "' value='" + albumId + "' />");
                
                /*
                    NAME        VALUE
                    i           *submit*
                    album_i     [albumTitle]

                */
                                
                // get tracks
                
                JSONArray jsonTrackArray = jsonObj.getJSONArray("tracks");
                               
                    
                // iterate over tracks and print out
                
                for (int j = 0; j < jsonTrackArray.length(); j++)
                {
                 
                    JSONObject jsonTrackObj = jsonTrackArray.getJSONObject(j);

                    String trackTitle = jsonTrackObj.getString("title");
                    
                    int jPlusOne = j + 1;
                    
                    out.print("<p>");
                    out.print("Track #" + jPlusOne + ": " + trackTitle);                    
                    out.print("</p>");

                }
                
            }
            
            //out.print(jsonArray.toString());

        %>
    
        </form>
        
    </body>
</html>
