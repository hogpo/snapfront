/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ACER-LAPTOP
 */
@WebServlet(name = "PostAddAlbumServlet", 
        urlPatterns = {"/PostAddAlbumServlet"})
public class PostAddAlbumServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, Exception 
    {
            response.setContentType("text/html;charset=UTF-8");
            System.out.println("Reached the PostAddAlbumServlet!");
            
            HttpPostMaster http = new HttpPostMaster();
            
            // retrieve data
            String exAlbumName = request.getParameter("album_name");
            String responseStringToConvert = http.sendGet("albums");
            JSONObject jsonObject = new JSONObject(responseStringToConvert);
            JSONArray jsonArray = (JSONArray) jsonObject.get("albums");
            JSONObject temp;
            
            //check album name doesn't already exist
            for(int i = 0; i < jsonArray.length(); i++)
            {
               temp = (JSONObject) jsonArray.get(i);
               if(temp.get("title").toString().equalsIgnoreCase(exAlbumName))
               {
                  String successMessage = "Error: Album " + exAlbumName + " already exists!";
                  request.setAttribute("success_message", successMessage);
                  request.getRequestDispatcher("success_or_failure.jsp").forward(request, response); 
               }
            }
            //If doesn't exist, create new album 
            String successMessage = "Album " + exAlbumName + " was created succesfully!";
            request.setAttribute("success_message", successMessage);
            request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);     
            
    
    }
       

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PostAddAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PostAddAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PostAddAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PostAddAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
