/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ACER-LAPTOP
 */
@WebServlet(name = "SearchAndDisplayAlbumOrTrackServlet", 
        urlPatterns = {"/SearchAndDisplayAlbumOrTrackServlet"})
public class SearchAndDisplayAlbumOrTrackServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, Exception 
    {
            response.setContentType("text/html;charset=UTF-8");
            System.out.println("We've reached the SearchAndDisplayAlbumOrTrackServlet!");
            
            //get parameters
            HttpPostMaster http = new HttpPostMaster();
            String exSearch = request.getParameter("search");
            String responseStringToConvert = http.sendGet("albums");
            JSONObject jsonObject = new JSONObject(responseStringToConvert);
            JSONArray jsonArray = (JSONArray) jsonObject.get("albums");
            
            
            // defaults to error page
            String sendToPage = "success_or_failure";
            String successMessage = "No search results found.";
            String found = "";
            String compare;
            int count = 0;
            if (request.getParameter("album") != null)
            {
                //searching for album
                for(int i = 0; i < jsonArray.length(); i++)
                {
                   JSONObject temp = (JSONObject) jsonArray.get(i);
                   compare = temp.get("title").toString().toLowerCase();
                   if(compare.contains(exSearch.toLowerCase()))
                   {
                      count++;
                      found += count + ". " + temp.get("title") + "<br />";
                   }    
                }
                // try to search
                //TODO
                //if result, view track
                if (count > 0)
                {                
                    request.setAttribute("album_name", found);
                    //request.setAttribute("album_duration", "INSERTTRACKDURATION");
                    sendToPage = "view_album.jsp";
                }
                else
                {
                   request.setAttribute("album_name", "Sorry, but '" + exSearch 
                           + "' did not return any results");
                   sendToPage = "view_album.jsp";
                }
            }
            else
            {
                for(int i = 0; i < jsonArray.length(); i++)
                {
                   JSONObject temp = (JSONObject) jsonArray.get(i);
                   String albumTitle = temp.get("title").toString();
                   JSONArray tracks = (JSONArray) temp.get("tracks");
                   compare = temp.get("title").toString().toLowerCase();
                   for(int j = 0; j < tracks.length(); j++)
                   {
                      temp = (JSONObject) tracks.get(j);
                      compare = temp.get("title").toString().toLowerCase();
                      if(compare.contains(exSearch.toLowerCase()))
                      {
                         count++;
                         found += count + ". " + temp.get("title") + ", " 
                                 + albumTitle + "<br />";
                      }
                   }
                }
                if (count > 0)
                {                
                    request.setAttribute("track_name", found);
                    //request.setAttribute("album_duration", "INSERTTRACKDURATION");
                    sendToPage = "view_track.jsp";
                }
                else
                {
                   request.setAttribute("track_name", "Sorry, but '" + exSearch 
                           + "' did not return any results");
                   sendToPage = "view_track.jsp";
                }
                
            }
            
            request.setAttribute("success_message", successMessage);
            
            request.getRequestDispatcher(sendToPage).forward(request, response);        
    
    }
        
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(SearchAndDisplayAlbumOrTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SearchAndDisplayAlbumOrTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(SearchAndDisplayAlbumOrTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SearchAndDisplayAlbumOrTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
