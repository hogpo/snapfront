/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ACER-LAPTOP
 */
@WebServlet(name = "PreEditTrackServlet", 
        urlPatterns = {"/PreEditTrackServlet"})
public class PreEditTrackServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws org.json.JSONException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, Exception 
    {
            response.setContentType("text/html;charset=UTF-8");
            System.out.println("We've reached the PreEditTrackServlet!");
            
            //unpack data
            
            String temp = request.getParameter("jsonArrayLength");
            
            int jsonArrayLengthInt = Integer.parseInt(temp);
            
            int selectedTrack = -1;
            
            for (int i = 0; i < jsonArrayLengthInt; i++)
            {
                
                String key1 = Integer.toString(i);
                
                if (request.getParameter(key1) != null)
                {
                   
                    String key2 = "id_" + i;
                    
                   String selAlbString = request.getParameter(key2);
                    
                    selectedTrack = Integer.parseInt(selAlbString);    
                    
                    System.out.println("success! " + i);
                }
                else
                {
                    System.out.println("failure! " + i);
                }
            }
            
            if (selectedTrack == -1)
            {
                //send to error page
                
                request.setAttribute("success_message", "Track not found.");
                
                request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
                
            }
            else
            {
                
                // get i id
                
                String albumId = request.getParameter("album_id");
                                
                String code = "albums/" + albumId + "/tracks/" + selectedTrack;
                
                HttpPostMaster http = new HttpPostMaster();
                
                String toConvert = http.sendGet(code);
                
                JSONObject selectedTrackAsJSON = new JSONObject(toConvert);
                
                /*
                
                {
                    "track":[
                       {
                          "updated_at":"2016-01-06 12:46:08",
                          "length":"100",
                          "created_at":"2015-12-15 15:49:16",
                          "album_id":"92",
                          "id":"486",
                          "title":"Alpha Mike Foxtrot"
                       }
                    ]
                }
                
                */
                
                JSONArray temp1 = selectedTrackAsJSON.getJSONArray("track");
                
                JSONObject jsonObj = temp1.getJSONObject(0);
                
                // String resultant = temp2.getString("title");
                //System.out.println("resultant (amf) = " + resultant);
                             
                // send to edit album page
                
                request.setAttribute("jsonObj", jsonObj);
                           
                request.getRequestDispatcher("edit_track.jsp").forward(request, response);      
            }
              
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PreEditTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PreEditTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PreEditTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PreEditTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
