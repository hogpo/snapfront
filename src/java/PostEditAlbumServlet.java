/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ACER-LAPTOP
 */
@WebServlet(name = "PostEditAlbumServlet", 
        urlPatterns = {"/PostEditAlbumServlet"})
public class PostEditAlbumServlet extends HttpServlet {

    private String successMessage;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, Exception 
    {
            response.setContentType("text/html;charset=UTF-8");
            System.out.println("We've reached the PostEditAlbumServlet!");
            
            //get required fields
            
            String albumIdString = request.getParameter("album_id");
            
            System.out.println("albumidstringt = " + albumIdString);

            HttpPostMaster http = new HttpPostMaster();
            
            // edit_name or delete
            
            if (request.getParameter("editing_name") != null)
            {
                
                //get name
                
                if (request.getParameter("edit_name") != null)
                {
                   
                    String newName = request.getParameter("edit_name");
                    
                    JSONObject changeJSON = new JSONObject();
                    
                    changeJSON.put("title", newName);
                    
                    String specificAlbumKey = "albums/" + albumIdString;
                    
                    // edit name

                    String feedback = http.sendPut(specificAlbumKey, changeJSON.toString());

                    JSONObject feedbackJSON = new JSONObject(feedback);

                    // this might have to be a string
                    boolean feedbackBoolean = feedbackJSON.getBoolean("success");

                    if (feedbackBoolean)
                    {
                        successMessage = "Album successfully edited! "
                                + "Name is now '" + newName + "'";
                    }
                    else
                    {
                        successMessage = "Problem editing album name. Please try "
                                + "again or contact your system administrator.";
                    }

                    request.setAttribute("success_message", successMessage);

                    request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
                }
                else
                {
                    //problem getting name
                    
                    successMessage = "Problem deleting album. Please try "
                            + "again or contact your system administrator.";
                
                
                    request.setAttribute("success_message", successMessage);

                    request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);

                }
            }
            else
            {
                // delete
                
                
                String feedback = http.sendDelete("albums/" + albumIdString);
                
                JSONObject feedbackJSON = new JSONObject(feedback);
                
                // this might have to be a string
                boolean feedbackBoolean = feedbackJSON.getBoolean("success");
                
                String successMessage;
                
                if (feedbackBoolean)
                {
                    successMessage = "Album successfully deleted!";
                }
                else
                {
                    successMessage = "Problem deleting album. Please try "
                            + "again or contact your system administrator.";
                }
                
                request.setAttribute("success_message", successMessage);
                
                request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
                
            }
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PostEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PostEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PostEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PostEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
