
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpPostMaster {

	public static void main(String[] args) throws Exception {

		HttpPostMaster http = new HttpPostMaster();
             

                /*
                
                // GET
                
		System.out.println("Testing 1 - Send Http GET request for albums");
		http.sendGet("albums");
                
                System.out.println("\nTesting 2 - Send Http GET request for specific album");
		http.sendGet("albums/92");
                
                System.out.println("\nTesting 3 - Send Http GET request for "
                        + "all tracks only froma specific album");
		http.sendGet("albums/92/tracks");
                
                System.out.println("\nTesting  - Send Http GET request for "
                        + "specific track off a specific album");
		http.sendGet("albums/92/tracks/486");
                
                // POST
		
		System.out.println("Testing A - Send Http POST request to input a new album");
		http.sendPost("albums", "title=Michaelangelo");
                
                System.out.println("\nTesting B - Send Http POST request to input a new track");
		http.sendPost("albums/106/tracks", "title=Stickz&length=99");
                
                // ADDEDIT
                
                JSONObject jsonObj = new JSONObject();
                
                jsonObj.put("title", "Geoffrey");
                
                String jsonObjToString = jsonObj.toString();
                                
                System.out.println("Testing C - Send Http PUT request to edit an album");
		http.sendPut("albums/106", jsonObjToString);
                
                System.out.println("\nTesting 1 - Send Http GET request for an album");
		http.sendGet("albums/106");
                
                //
                
                JSONObject jsonObj = new JSONObject();
                
                jsonObj.put("title", "Stonz");
                jsonObj.put("length", "101");
                
                String jsonObjToString = jsonObj.toString();
                                
                System.out.println("Testing D - Send Http PUT request to edit a track");
		http.sendPut("albums/106/tracks/512", jsonObjToString);
                
                System.out.println("\nTesting 1 - Send Http GET request for a track");
		http.sendGet("albums/106/tracks/512");
        
                
                // DELETE
                
                System.out.println("\nTesting (i) - Send Http DELETE request for a track");
		http.sendDelete("albums/106/tracks/512");
                
                System.out.println("\nTesting 1 - Send Http GET request for albums");
		http.sendGet("albums/106");
                
                //
                
                System.out.println("\nTesting (ii) - Send Http DELETE request for an album");
		http.sendDelete("albums/106");
                
                System.out.println("\nTesting 1 - Send Http GET request for all albums");
		http.sendGet("albums");

                */  

	}

	// HTTP GET request
	public String sendGet(String code) throws Exception {

		String url = "http://178.62.37.42/" + code;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("api-username", "george");
                con.setRequestProperty("api-token", "d298467d328d41ff16fd3eee3c7002a4");

		int responseCode = con.getResponseCode();
		System.out.println("Sending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());

                return response.toString();
                
	}
	
	// HTTP POST request
	public String sendPost(String code, String urlParams) throws Exception {

		String url = "http://178.62.37.42/" + code;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                
		//add reuqest method
		con.setRequestMethod("POST");

		//add reuqest headers                
		con.setRequestProperty("api-username", "george");
                con.setRequestProperty("api-token", "d298467d328d41ff16fd3eee3c7002a4");

		String urlParameters = urlParams;
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		//print result
		System.out.println(response.toString());
                
                return response.toString();

	}
        
	// HTTP PUT request
	public String sendPut(String code, String jsonObjToString) throws Exception {
            
		String url = "http://178.62.37.42/" + code;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                
		//add request method
		con.setRequestMethod("PUT");

		//add reuqest headers                
		con.setRequestProperty("api-username", "george");
                con.setRequestProperty("api-token", "d298467d328d41ff16fd3eee3c7002a4");

                con.setRequestProperty("content-type", "application/json");
                
		String urlParameters = jsonObjToString;
		
		// Send post request
		con.setDoOutput(true);
                               
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
                wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'PUT' request to URL : " + url);
		System.out.println("Put jsonObjToString : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		//print result
		System.out.println(response.toString());
                
                return response.toString();

	}
        
        // HTTP GET request
	public String sendDelete(String code) throws Exception {

		String url = "http://178.62.37.42/" + code;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("DELETE");

		//add request header
		con.setRequestProperty("api-username", "george");
                con.setRequestProperty("api-token", "d298467d328d41ff16fd3eee3c7002a4");

		int responseCode = con.getResponseCode();
		System.out.println("Sending 'DELETE' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
                
                return response.toString();

	}

}