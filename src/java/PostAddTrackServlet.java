/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ACER-LAPTOP
 */
@WebServlet(name = "PostAddTrackServlet", 
        urlPatterns = {"/PostAddTrackServlet"})
public class PostAddTrackServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, Exception 
    {
            response.setContentType("text/html;charset=UTF-8");
            System.out.println("Reached the PostAddTrackServlet!");
            
            //get user input            
            String exAlbumName = request.getParameter("album_name");
            String exTrackName = request.getParameter("track_name");
            
            //validate user input          
            HttpPostMaster http = new HttpPostMaster();
            String responseStringToConvert = http.sendGet("albums");
            JSONObject jsonObject = new JSONObject(responseStringToConvert);
            JSONArray jsonArray = (JSONArray) jsonObject.get("albums");
            JSONObject temp;
            JSONObject sAlbum = null;
            String successMessage;
            //Check to see if the album already exists
            //If yes, retrieve the album id
            //If no, redirect the user 
            for(int i = 0; i < jsonArray.length(); i++)
            {
               temp = (JSONObject) jsonArray.get(i);
               if(temp.get("title").toString().equalsIgnoreCase(exAlbumName))
               {
                  sAlbum = temp;
               }
               //JSONArray tee = (JSONArray) temp.get("tracks");
            }
            if(sAlbum == null)
            {
               successMessage = "ERROR: Album " + exAlbumName + " does not exist";
               request.setAttribute("success_message", successMessage);
               request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
            }
            //Check to see if track exists in album
            jsonArray = (JSONArray) sAlbum.get("tracks");
            for(int i = 0; i < jsonArray.length(); i++)
            {
               JSONObject d = (JSONObject) jsonArray.get(i);
               if(d.get("title").toString().equalsIgnoreCase(exTrackName))
               { 
                  successMessage = "ERROR: Track " + exTrackName + " already exists on " 
                          + exAlbumName;
                  request.setAttribute("success_message", successMessage);
                  request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
               }
            }
            //Add track to chosen album 
            //N.B. LENGTH is a default value - NEED TO CHANGE
            System.out.println("\nTesting B - Send Http POST request to input a new track");
		http.sendPost("albums/" + sAlbum.get("id") + "/tracks", "title=" + 
                        exTrackName + "&length=99");
            successMessage = exTrackName + " succesfully inserted into album " 
                             + exAlbumName + ".";
            request.setAttribute("success_message", successMessage);
            request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
    }
   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PostAddTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PostAddTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PostAddTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PostAddTrackServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
