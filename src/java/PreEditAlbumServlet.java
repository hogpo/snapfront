/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ACER-LAPTOP
 */
@WebServlet(name = "PreEditAlbumServlet", 
        urlPatterns = {"/PreEditAlbumServlet"})
public class PreEditAlbumServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, Exception 
    {
            response.setContentType("text/html;charset=UTF-8");
            System.out.println("We've reached the PreEditAlbumServlet!");
            
            //unpack data
            
            String temp = request.getParameter("jsonArrayLength");
            
            int jsonArrayLengthInt = Integer.parseInt(temp);
            
            int selectedAlbum = -1;
            
            for (int i = 0; i < jsonArrayLengthInt; i++)
            {
                
                String key1 = Integer.toString(i);
                
                if (request.getParameter(key1) != null)
                {
                   
                    String key2 = "id_" + i;
                    
                   String selAlbString = request.getParameter(key2);
                    
                    selectedAlbum = Integer.parseInt(selAlbString);    
                    
                    System.out.println("success! " + i);
                }
                else
                {
                    System.out.println("failure! " + i);
                }
            }
            
            if (selectedAlbum == -1)
            {
                //send to error page
                
                request.setAttribute("success_message", "Album not found.");
                
                request.getRequestDispatcher("success_or_failure.jsp").forward(request, response);
                
            }
            else
            {
                
                // get i id
                
                String code = "albums/" + selectedAlbum;
                
                HttpPostMaster http = new HttpPostMaster();
                
                String toConvert = http.sendGet(code);
                
                JSONObject selectedAlbumAsJSON = new JSONObject(toConvert);
                
                /*
                     
                System.out.println("toConvert = " + selectedAlbumAsJSON.toString());
                                
                JSONObject temptemp = selectedAlbumAsJSON.getJSONObject("tracks");
                 */               
                // send to edit album page
                
                request.setAttribute("selectedAlbum", selectedAlbumAsJSON);
                
                System.out.println("selectedalbu, === " + selectedAlbumAsJSON.toString());
                            
                request.getRequestDispatcher("edit_album.jsp").forward(request, response);      
            }
            
            
           
               
              
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PreEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PreEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(PreEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PreEditAlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
